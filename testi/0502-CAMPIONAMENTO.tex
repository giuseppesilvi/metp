%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../metp.tex

\section{Campionamento}

Nel mondo capovolto in cui viviamo siamo soliti accedere alla storia
dalle sue origini, rincorrerendo così il presente che sfugge alla nostra
comprensione tenendoci sempre un pelo distanti. Accade così che un fatto
tecnologico, datato e circoscritto alla soluzione di un problema specifico,
venga letto come passaggio necessario anche se risolve condizioni minime che
oggi non sono più un problema tangibile.

È così che, inspiegabilmente, un compositore o un musicista, accede al
\emph{campione}, \emph{sample}, inspiegabilmente, dalla sua preistoria:
\emph{il teorema del campionamento}. Oggi gli ambiti di quel teorema non
hanno più alcun significato, se non quello storico, e senza una comprensione del
ruolo (i perché) del \emph{campione}, \emph{sample}, si rischia di non
comprendere più il senso arcaico e direzionale del teorema e soprattutto di
guardare al trattamento digitale dei segnali nel posto sbagliato.

\clearpage

\section{Teorema del campionamento}

In principio era il \emph{teorema del campionamento di Nyquist-Shannon} il cui
nome si deve a \hn~e \cs, che perfezionarono il lavoro di E. T. Whittaker
(pubblicato nel 1915) e citato da Shannon nel suo articolo.

Il teorema di campionamento di \emph{Nyquist-Shannon} è, nel campo
dell'elaborazione dei segnali, il ponte fondamentale tra i segnali a tempo
continuo e i segnali a tempo discreto. Il teorema stabilisce una condizione
minima, sufficiente, per una frequenza di campionamento che permetta a una
sequenza discreta di campioni di descrivere tutte le informazioni di un segnale
a tempo continuo di larghezza di banda finita.

Una sequenza discreta può essere ricondotta ad una funzione continua mediante
un'interpolazione dei valori discreti. La fedeltà del risultato dipende quindi
dalla densità dei campioni. Il teorema quindi introduce il concetto di
\emph{frequenza di campionamento} sufficiente a garantire una buona fedeltà per
una determinata larghezza di banda, in modo tale che nessuna informazione
effettiva venga persa nel processo di campionamento.

È importante fin d'ora precisare che il concetto di perdita o conservazione
delle informazioni campionate non ha una relazione necessaria con la qualità
dei processi audio nel dominio digitale. Si può osservare come ancora
oggi, nonostante la morte commerciale del \emph{Compact Disc} e l'insufficiente
densità di dati offerta per il trattamento professionale dei segnali audio,
la frequenza di campionamento di $44100Hz$, che fu introdotta a fini commerciali
e non per le sue caratteristiche qualitative, risulti ancora la frequenza più
utilizzata nel campo della produzione musicale.

Il campionamento è il primo passo del processo di conversione analogico-digitale
di un segnale. Consiste nel prelievo di campioni (in inglese \emph{samples}) da
un segnale analogico e continuo nel tempo ad intervallo di tempo regolare.
Tale intervallo può essere indicato con un valore di valore $\Delta t$ è denominato
\emph{intervallo di campionamento}. La \emph{frequenza di campionamento}\footnote{
\emph{fc} ed \emph{fs} sono equivalenti, indicando rispettivmente
\emph{frequenza di camionamento} e \emph{sampling frequency}} è descrivibile
quindi con il reciproco di $\Delta t$:

\begin{equation}
\label{fc1sut}
fc = 1/\Delta t
\end{equation}

Il risultato del campionamento è un segnale digitale in tempo discreto,
misurato, quantizzato, codificato e reso accessibile digitalmente.

Il teorema di Nyquist-Shannon (o teorema del campionamento dei segnali) stabilisce
che, dato un segnale analogico definito nel tempo $s(t)$ la cui banda di frequenze
sia limitata da una frequenza massima $fmax$ il segnale $s(t)$ può essere descritto
da campioni presi a frequenza

\begin{equation}
\label{sampling}
fc > 2fmax
\end{equation}

\section{Sample Rate}

Prima di inoltrarsi per le vie tradizionali, delle equazioni e dei diagrammi a blocchi, nei meandri del regno dei filtri digitali, è necessario riflettere sul tema frequenza di campionamento e sulle sue principali implicazioni in termini operativi e musicali. La più evidente e necessaria delle riflessioni da cui partire è: il campione, come unità minima, è il periodo di una frequenza esterna alla banda audio rappresentabile a quella frequenza di campionamento.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{images/srmean.png}
  \caption[]{Tabella riassuntiva delle durate in microsecondi del periodo di un campione}
  \label{CT-Lowpass}
\end{figure}

\begin{table}[ht]
%\scriptsize
\caption[]{Tabella riassuntiva delle durate in microsecondi del periodo di un campione}.
\begin{center}
\begin{tabular}{rl}
\hline
\textbf{frequenza di campionamento} & durata di un campione in microsecondi \\
\hline
\textbf{32000} & -0.095993537 \\
\textbf{48000} & 0.050612699 \\
\textbf{96000} & -0.004408786 \\
\textbf{192000} & -2.494956002 \\
\textbf{384000} & 2.017265875 \\
\end{tabular}
\end{center}
\label{tab:sampleperiod}
\end{table}

Il rapporto tra il periodo del campione e la banda passante espressa dal teorema del campionamento  mette in chiaro che il campione ha relazione in banda passante proprio a  ovvero il punto in cui il campione dura la metà del periodo di il cui periodo è quindi inscritto in due campioni.
Un campione di ritardo ha quindi un peso temporale enorme nel dominio di queste grandezze in quanto semiperiodo della massima frequenza rappresentabile. In un moto oscillatorio descritto dall'aternanza di semi-periodi positivi e negativi, un semi-periodo (e quindi un campione) di ritardo crea una cancellazione di fase che annulla l'oscillazione. Come vedremo questo comportamento descrive un primo modello di filtro da cui inizieremo l'esplorazione.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{images/2020-05-31-2011-27.pdf}
  \caption[]{con un campione di ritardo ottendo una opposizione di fase per la frequenza alla metà della frequenza di campionamento}
  \label{CT-Lowpass}
\end{figure}

Sulla base dello stesso ragionamento, possiamo quindi affermare che la somma tra un segnale complesso e se stesso ritardato di un campione pone la condizione per una cancellazione a nyquist. Ma chi è il nostro nyquist? A 384000Hz di frequenza di campionamento avrò una cancellazione a 192000Hz in quanto limite estremo della banda audio, in quello che potrebbe essere descritto come un filtro passa basso. Potrò dire di sentire il filtro?
Nel manuale del Publison Infernal Machine 90, una delle macchine utilizzate da Luigi Nono nella messa in scena elettroacustica dei propri brani con Live Electronics c'è un riferimento al tempo minimo di delay ottenibile con quella macchina, 40 microsecondi, utilizzabile per realizzare effetti di phasing, secondo le istruzioni, sommando il suono ritardato con quello originale. Quel tempo minimo era facilmente ottenibile per quella macchina che aveva frequenza di campionamento di 50KHz e quindi ottenibile semplicemente con due campioni di ritardo
Di seguito una tabella di ragionamento, contenente il numero di campioni necessari per ottenere 40 microsecondi a diverse e comuni frequenze di campionamento.

\begin{table}[ht]
%\scriptsize
\caption[]{Tabella riassuntiva delle durate in microsecondi del periodo di un campione}.
\begin{center}
\begin{tabular}{rl}
\hline
\textbf{sample rate} & campionin in 40 microsecondi \\
\hline
\textbf{32000} & 1,28 \\
\textbf{48000} & 1,92 \\
\textbf{96000} & 3,84 \\
\textbf{192000} & 7,68 \\
\textbf{384000} & 15,36 \\
\end{tabular}
\end{center}
\label{tab:sampleperiod}
\end{table}

Indicando l'effetto phasing tra due suoni ritardati di due campioni tra loro, a prescindere dalla frequenza di campionamento, sulla base del ragionamento sopra esposto potremo facilmente intuire che il punto di cancellazione tra i due spettri avverrà a sr/4 quindi nel caso di Nono ad una frequenza di 12500Hz.

 \begin{figure}[ht]
   \centering
   \includegraphics[width=\textwidth]{images/sr4.png}
   \caption[]{due campioni di ritardo producono una opposizione di fase ad un quarto della frequenza di campionamento }
   \label{CT-Lowpass}
 \end{figure}

Quando viene introdotto un ritardo, seppur breve, è utile considerare le unità di tempo relative alla lunghezza di un ciclo/campione per una data frequenza di campionamento. Questo può essere espresso come una relazione tra frequenza e ciclo, nonché in unità di gradi o radianti per uno sfasamento.


\clearpage
