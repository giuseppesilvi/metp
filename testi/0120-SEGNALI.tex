%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
% !TEX root = ../metp.tex

\section{Segnale}

- \emph{Ciao!} - una mano alzata in segno di saluto.

L'essere è vivente in segnali. Animale o vegetale che sia, vive mediante diversi
livelli di relazione con i segnali. Ho letto di una pianta di tabacco selvatico
che cresce nello Utah che si difende dall'attacco delle larve di una falena
aiutando i predatori a individuare le loro prede, indicandogli chimicamente
esattamente dove esse si trovino. Anche le prede sono sensibili al segnale e,
una volta emesso, le falene di solito evitano di deporre le loro uova su una
pianta già attaccata.
% https://www.lescienze.it/news/2001/03/19/news/segnali_vegetali-591553/
%
% Così lo scorpione di Esopo che doveva attraversare il fiume, non sapendo
% nuotare, chiese aiuto alla rana.
%
% - Fossi matta! Così appena siamo in acqua mi pungi e mi uccidi!
%
% - E per quale motivo dovrei farlo? Tu moriresti ed io annegherei!
%
% Titubante, lo caricò sul dorso. A metà tragitto la rana fu punta dallo scorpione
% e, morendo, chiese il perché del folle gesto al suo passeggero.
%
% - Perché sono uno scorpione\ldots~È la mia natura!"
%
% Lo scorpione, consapevole che pungendo la rana annegherà anche lui, non riesce
% a trattenersi quando un segnale interno, una vocina, gli ricorda quale sia la sua
% natura.

Gli animali sono in grado di selezionare la tipologia di informazione utile per
avere adeguate risposte comportamentali. Questi segnali scatenanti vengono
filtrati da un meccanismo altamente complesso.

% https://www.studocu.com/it/document/universita-degli-studi-di-ferrara/zoologia/appunti-di-lezione/zoologia-i-segnali/6427125/view

Le rane producono una vasta gamma di segnali, in particolare nella loro stagione
riproduttiva, e mostrano diversi tipi di comportamenti complessi per attirare i
compagni, per respingere i predatori e per sopravvivere in generale.

% - https://it.qaz.wiki/wiki/Frog

La rana di portorico è chiamata \emph{coqui} perche il maschio emette un \emph{co}
e un \emph{qui} a due frequenze diverse. Nonostante sia in grado di produrre due
suoni diversi, il maschio percepisce solo il \emph{co}, ma non il \emph{qui}.
In questo modo un maschio può riconosce che in quella zona c’è un altro maschio
che marca il territorio. La femmina invece sente solo il \emph{qui}, ovvero il
segnale che un maschio marca il territorio. L’organo uditivo dei maschio
percepisce ad una frequenza differente da quello delle femmine, in una forma di
filtrazione periferica all’interno della specie. Gli organi di senso di un essere
vivente agiscono già quindi come sistemi di filtrazione (periferica). D'altra
parte l'uomo per esempio percepepisce moltissimi stimoli che attraversano
la filtrazione periferica e che poi il sistema centrale filtra, determinando
quali siano quelli effettivamente importanti per emettere una risposta. Una
selezione degli stimoli per indurre una risposta comportamentale.

Prima ancora di sapere cosa sia un segnale sappiamo già differenziarlo in:

\begin{description}
\item[segnale d’uscita:] l'informazione prodotta da un dato sistema fisico
  (il \emph{co-qui}).
\item[segnale d’ingresso:] la sollecitazione su un dato sistema fisico prodotta
  da un'informazione (il \emph{co} o il \emph{qui}).
\end{description}

% https://www.studocu.com/it/document/universita-degli-studi-di-ferrara/zoologia/appunti-di-lezione/zoologia-i-segnali/6427125/view

L'uomo riceve entrambi i segnali \emph{co-qui} e può osservarne il significato
per un'altra specie animale e attarverso l'intricata rete di filtrazioni sulle
informazioni rilevate arrivare a dire che tutti i rospi sono rane, ma non tutte
le rane sono rospi.

%https://it.qaz.wiki/wiki/Frog
%La filtrazione periferica= organi di senso Filtrazione centrale= sistema nervoso

La parola tedesca \emph{umwelt} significa \emph{ambiente sensoriale in cui si è
immersi}. L'uomo di oggi ha una visione tricromatica, ma i suoi antenati avevano
una visione bicromatica. Dal punto di vista evolutivo assumere una visione
tricromatica ha rappresentato un traguardo eccezionale.

Ci concediamo un'ultima analisi generica sui segnali descrivendo ancora due
situazioni.

All'interno di un organismo i segnali chimici possono avere natura ormonale o
nervosa che, attraverso appropriati sistemi di trasmissione determinano uno stimolo
sulle strutture competenti (recettore). L'impulso nervoso per il muscolo, la
ghiandola in secrezione: sistemi di comunicazione, reti di informazioni interne,
che poi possono diventare interno-esterno (segnale d'uscita) e ancora esterno-esterno,
in una grandissima varietà di aspetti e combinazioni che sono alla base della vita
di relazione.

Gli esseri viventi sono in grado anche di tradurre segnali provenienti dall'ambiente
esterno e, su questa capacità, si basa l'adattamento. La luce, la sua qualità,
la sua durata, la sua intensità, parametri di controllo, segnali, che regolano
la vita, imparando così a vedere l'ambiente come sorgente di innumerevoli segnali
d'ingresso.

% è così che si comporta un'opera d'arte ecosistemica di Agostino Di Scipio.

\subsection{Definizioni di segnali}

Nella Divina Commedia la parola segnale compare con il valore di \emph{segno},
\emph{indizio}, nel sintagma \emph{far segnale}: (Fiore CXXXVII 5)
\emph{non ne fece segnale}, cioè \emph{non ne diede segno}, \emph{non lo lasciò
intendere}.

\begin{etimo}[Segnale]
  Dal latino \emph{signum}, in origine \emph{intaglio}; prima metà sec. XIII.
  Latino tardo \emph{signāle}, neutro sostantivo dell'aggettivo \emph{signālis} “che segna”.
\end{etimo}

% Genericamente, indicazione di tipo ottico o acustico, per lo più stabilita d’intesa o convenzionale, con cui si dà una comunicazione, un avvertimento, un ordine a una o più persone. Concretamente, qualsiasi oggetto, strumento o dispositivo usato per fare segnalazioni; in particolare, l’informazione contenuta nella trasmissione del messaggio, qualunque sia il mezzo trasmissivo.
%
% SOS Nelle telecomunicazioni, segnale radiotelegrafico marittimo di soccorso, adottato internazionalmente dal 1908, in quanto i 3 segnali che corrispondono nell’alfabeto Morse (... − − − ..., cioè 3 punti, 3 linee, 3 punti), sono di facile trasmissione e di chiara ricezione; le 3 lettere sono state poi interpretate come iniziali delle parole inglesi save our souls. Speciali accordi internazionali fissano determinati periodi di silenzio radiotelegrafico nelle 24 ore di ogni giorno, per rendere possibile la ricezione di eventuali segnali di questo tipo, come pure impongono l’adozione di speciali apparecchi radioriceventi automatici (auto-allarme), per la loro ricezione.
%
% detezione del segnale Risposta cognitiva alla presentazione di uno stimolo sensoriale che definisce la percezione da parte di un individuo. La teoria della d. del s. afferma che nella percezione di un segnale uditivo, visivo ecc., la semplice misurazione della soglia sensoriale, cioè la risposta degli organi di senso a un determinato stimolo (per es., udibilità di un suono), non è del tutto informativa, ma è necessario anche analizzare le modalità soggettive di interpretazione e di risposta (criterio di risposta), ovvero in che modo le informazioni percepite sul piano fisico vengono elaborate. La d. del s. è quindi il risultato dell’integrazione tra la risposta fisica a uno stimolo e i processi cognitivi individuali.
%
% segnale causale funzione dipendente dal tempo che rappresenta uno stimolo esterno su un sistema fisico. Matematicamente, un segnale causale è rappresentato mediante una funzione nulla per t < 0. L’istante t = 0 rappresenta il momento in cui viene applicato dall’esterno il segnale (→ funzione gradino).
%
%
%
% segnale Indicazione di tipo ottico o acustico o olfattivo (in animali e insetti), con cui si dà una comunicazione, un avvertimento, un ordine. In un senso più esteso, mutuato dalla biologia, il s. è costituito da ogni comunicazione semplice o complessa trasmessa da un punto a un altro di una cellula, o di un organismo, oppure da uno a un altro individuo, o ancora dall’ambiente agli organismi. In psicologia, si parla di s. verbali e non verbali e di espressione facciale delle emozioni come strumenti comunicativi (➔ comunicazione, meccanismi psicologici della) e di detezione del s. per indicare i processi cognitivi di attenzione e decodifica dei segnali. In psicoanalisi, si parla di s. d’angoscia per indicare il vissuto dell’Io di fronte a un pericolo, che permette l’innesco di un meccanismo di difesa. I s. cerebrali consistono in onde elettriche prodotte dal cervello in condizioni di riposo e sotto stimolo, che possono essere interpretate come correlato anatomofunzionale dell’attività di elaborazione delle informazioni da parte di grandi popolazioni di neuroni (➔ segnali cerebrali, analisi dei).

Possiamo stilare una lista sconfinata di esempi comuni di segnali utilizzati nella
vita quotidiana, da quelli acustici semplici di suonerie, cicalini di avvertimento,
a quelli complessi provenienti da strumenti musicali. Segnali luminosi, di
avvertimento, una luce lampeggiante, o di istruzione come un semaforo rosso, fino
ai segnali prodotti da macchine che in conseguenza e relazione a variazioni di
fisiche e che usiamo per tradurre campi fisici diversi come il sismografo per
esempio che crea un debole segnale elettrico di cui abbiamo la chiara visione
stampata su carta.

Partendo dall'etimologia latina del termine \emph{signale}, sostantivo di
\emph{signalis} derivato da \emph{signum}, \emph{segno}, definiamo genericamente
un'indicazione sensoriale convenzionale, stabilita d'intesa, per dare una
comunicazione, un avvertimento, un ordine. Esempi ne sono le espressioni:
\emph{dare il segnale}, \emph{attendere il segnale} oppure le innumerevoli
indicazioni dei segnali stradali.

Una comunicazione, quindi, di cui se ne condividano le modalità e se ne
comprendano i contenuti: il segnale si fa \emph{portatore di una informazione}
che giustifica l'esistenza e l'importanza del segnale stesso. Una lampadina ed un
semaforo, pur appartenenti alla categoria dei segnali luminosi, assumono
significati diversi.

Sulla base di queste considerazioni possiamo dire che un segnale \emph{è una
qualunque grandezza fisica variabile cui è associata una informazione.} In molti
casi del segnale si sanno molte cose, in funzione di quanto siamo in grado di
annotare o registrare, su carta, su nastro magnetico, dentro un file digitale.

Oltre la portata sensoriale, ovvero direttamente percepita dai sensi, possiamo definire
una comunicazione attraverso segnali a distanza come nella \emph{tele-comunicazione},
dall'etimologia greca del termine \emph{têle}, \emph{lontano}. Da lontano
trasmettiamo e riceviamo il segnale radio, un campo elettromagnetico variabile.
Nelle telecomunicazioni definiamo segnale \emph{la variazione in funzione del tempo
di una grandezza fisica}, utilizzata per convogliare informazioni dello stato
fisico di un sistema. La funzione matematica di una o più variabili nel tempo
diventa quindi lo strumento ottimale di descrizione, rappresentazione ed
elaborazione del segnale.

Prima di inoltrarsi nella descrizione delle tipologie di segnali, è necessario
precisare che il segnale come rappresentazione, seppur estremamente dettagliato,
veicolo di un'enormità di informazioni, è pur sempre una rappresentazione. Il
ragionamento si fa più che necessario se consideriamo che spesso ci troviamo in
condizioni di lavorare su segnali che sono rappresentazioni di rappresentazioni.
Un segnale acustico strumentale, uno squillo di tromba, nella sua rappresentazione
elettrica cede delle informazioni. Lo stesso segnale elettrico può diventare a
sua volta segnale digitale dello stesso fenomeno acustico, in una rappresentazione
della rappresentazione. Questa perdita di informazioni deve costantemente rimanere
sorvegliata e considerata parte delle nostre capacità di trattamento dei segnali,
nel rispetto della significativa differenza tra l'informazione fisica ed una sua
qualsiasi rappresentazione.

Ci sono molti esempi di segnali di segnali a cui siamo abituati e che non analizziamo
mai nell'ottica della perdita. Su un set cinematografico un'azione viene ripresa
da una cinepresa \emph{IMAX} con pellicola da $70mm$ (dati riferiti all'ambito
di massima qualità disponbile). Il meccanismo della ripresa in pellicola di
un'azione genera un segnale, quello impresso sulla pellicola, fatto di un numero
finito di fotogrammi al secondo. Tra un fotogramma ed il successivo non ci sono
informazioni. La visione al proiettore cinematografico di quella pellicola crea
un flusso continuo ai nostri occhi, ed il segnale registrato acquisice movimento
(apparentemente) fluido su una decina di metri di schermo (una realtà continua,
diversa da quella \emph{originale}). La stessa azione la possiamo poi rivedere a
casa, dopo qualche tempo, in un segnale diverso da quello cinematografico che
è diverso dall'azione originale, in una rappresentazione domestica, della
rappresentazione cinematografica, dell'azione iniziale.

\subsection{Tipologie di segnali}

Abbiamo descritto il segnale come veicolo di informazioni nel tempo.

Le grandezze che misuriamo nel tempo descritte dal segnale possono essere
distinte in due tipologie:

\begin{description}
  \item[segnali a tempo continuo] in cui la grandezza variabile nel tempo può
  assumere con continuità tutti i valori compresi entro un certo intervallo,
  eventualmente illimitato. La variabile temporale assumerà il nome di $t$ mentre
  il segnale sarà indicato con $x(t)$, $y(t)$.
  \item[segnali a tempo discreto] sono delle successioni, delle sequenze $x[n]$,
  $y[n]$ dove la variabile tempora $n$ è il numero intero rappresentante la posizione
  all'interno della successione.
\end{description}

Le variazioni delle grandezze misurate sul \emph{codominio} della funzione
che rappresenta il segnale può essere, secondo le due categorie sopra esposte, di
altrettante tipologie:

\begin{description}
  \item[segnali ad ampiezza continua] che possono assumere con continuità tutti
  i valori reali di un intervallo (anche illimitato) come nel caso di un segnale
  acustico, o in generale nei segnali osservati nei sistemi naturali.
  \item[segnali ad ampiezza discreta] aventi un numero finito di misurazioni,
  quindi appartenenti ad un insieme numerabile, eventualmente illimitato.
  Il comportamento della luce del semaforo, o in generale dei sistemi binari, può
  assumere solo due stati, acceso spento.
\end{description}

Le caratteristiche dei segnali descritti definiscono quindi le due categorie:

\begin{description}
  \item[segnale analogico] a tempo continuo e ad ampiezza continua
  \item[segnale digitale] (o numerico) a tempo discreto e ad ampiezza quantizzata
\end{description}

La musica elettronica viene generalmente prodotta utilizzando un computer,
sintetizzando o elaborando segnali audio digitali. Queste sono sequenze di numeri,

\begin{equation}
  \label{digsig}
  ...,x[n-1],x[n],x[n+1],...
\end{equation}

dove l'indice $n$ definisce, per numeri interi, la posizione del campione (il singolo numero),
nel segnale digitale (la sequenza di numeri).

Un segnale può anche essere periodico o non periodico, si dice periodico quando
una parte di questo si ripete nel tempo uguale a se stessa. L'intervallo di tempo
in cui si ripete la parte è detto periodo. Un esempio di segnale audio digitale
periodico è la \emph{sinusoide}:

\begin{equation}
  \label{digsin}
x[n] = a cos(\omega n + \phi)
\end{equation}

dove $a$ è l'ampiezza, $\omega$ è la frequenza angolare e $\phi$ è la fase
iniziale. La fase è una funzione del numero di campione $n$, pari a $\omega n + \phi$
La fase iniziale è la fase nel campione zero (n = 0).

% La Figura 1.1 (parte a) mostra graficamente una sinusoide. L'asse orizzontale mostra i valori successivi di n e l'asse verticale mostra i valori corrispondenti di x [n]. Il grafico è disegnato in modo tale da enfatizzare la natura campionata del segnale. In alternativa, potremmo disegnarlo più semplicemente come una curva continua (parte b). Il disegno superiore è la rappresentazione più fedele della sinusoide (audio digitale), mentre quella inferiore può essere considerata una sua idealizzazione.
% I sinusoidi svolgono un ruolo chiave nell'elaborazione audio perché, se si sposta uno di essi a sinistra o a destra di un numero qualsiasi di campioni, ne si ottiene un altro. Ciò semplifica il calcolo dell'effetto di tutti i tipi di operazioni sui sinusoidi. Le nostre orecchie usano questa stessa proprietà speciale per aiutarci ad analizzare i suoni in arrivo, motivo per cui le sinusoidi e le combinazioni di sinusoidi possono essere utilizzate per ottenere molti effetti musicali.
% I segnali audio digitali non hanno alcuna relazione intrinseca con il tempo, ma per ascoltarli dobbiamo scegliere una frequenza di campionamento, di solito dato il nome della variabile R, che è il numero di campioni che rientrano in un secondo. Il tempo t è correlato

% Nelle telecomunicazioni, dal punto di vista del tipo di informazione trasportata
% fino all'utente si può distinguere essenzialmente tra:
%
% \begin{description}
% \item[segnale audio]
% \item[segnale video]
% \item[segnale dati]
% \end{description}
%
% ciascuno con caratteristiche diverse in termine di banda di trasmissione richiesta.
%
% Dal punto di vista della tipologia fisica del segnale si ha:
%
% \begin{description}
% \item[segnale elettrico]
% \item[segnale elettromagnetico]
% \item[segnale acustico]
% \end{description}

% In elettronica un segnale viene dunque studiato attraverso un modello matematico
% o funzione in cui il tempo (o il suo inverso, la frequenza) è considerato
% variabile indipendente.
%
% La teoria dei segnali studia la rappresentazione dei segnali in modo da poter poi
% manipolarli e trattarli matematicamente.

\subsection{Il segnale elettrico}

L’operazione che traduce un‘onda acustica in un segnale elettrico è chiamata
\emph{trasduzione}, ed è la funzione che svolgono i trasduttori, di cui fanno
parte anche i microfoni. \cite{ps:01}

\begin{figure}[h]
\centering
\includegraphics[width=0.75\columnwidth]{images/0200/trasduzione.png}
\caption[]{Generalizzazione di un trasduttore.}
\label{sig:trasduzione}
\end{figure}

Ad una compressione dell’onda sonora corrisponde un valore positivo di tensione,
mentre, ad una rarefazione, un valore negativo. Il segnle acustico tempo-continuo
viene quindi tradotto in un segnale elettrico analogo, analogico. Maggiore sarà
la precisione del trasduttore migliore sarà la rappresentazone elettrica dell'onda
sonora originale.

La trasduzione induce anche al cambio linguistico: il segnale sonoro, udibile,
diviene elettrico, inudibile, che definiamo \emph{segnale audio}.

In quanto segnale elettrico analogico si muove su conduttori metallici. Le
connessioni audio devono essere schermate per proteggere i segnali dal rumore
indotto da campi magnetici esterni: il conduttore, isolato, viaggia all’interno 
di uno schermo elettrico. Una volta collegato il cavo audio, mediante appositi
connettori, ai dispositivi di trasmissione e ricezine (per esempio un microfono
ad un mixer) il segnale verrà gestito in modo che la schermatura venga assorbita
da uno dei dispositivi che sarà a sua volta collegato alla messa a terra
(potenziale elettrico zero) della rete elettrica. 

\begin{figure}[h]
\centering
\includegraphics[width=0.75\columnwidth]{images/unbaline1.png}
\caption[]{Linea sbilanciata}
\label{sig:unbaline}
\end{figure}

Questo tipo di connessione si avvale dello schermo come conduttore di ritorno
(polo freddo) e costituisce una \emph{linea sbilanciata}. Per le sue
caratteristiche elettriche questo tipo di collegamento è adatto solo per
percorsi brevi (pochi metri) e in ambienti esenti da campi magnetici e
interferenze in alta frequenza (disturbi radio). La linea sbilanciata
è per esempio di uso comune per collegare le chitarre elettriche ai pedali, agli
amplificatori. Nei dispositivi analogici complessi, negli strumenti di elaborazione
del segnale analogico (a partire dal semplice controllo del volume in un mixer)
il segnale elaborato è sempre sbilanciato. Il motivo di ciò è insito nel fatto
che ogni operazione diretta su una linea bilanciata dovrebbe intervenire
simultaneamente, e perfettamente (mantenendo integra l'opposizione di fase), su
entrambi i segnali.

La linea sbilanciata è inoltre usata per interfacciare strumenti di fascia bassa,
\emph{consumer}, esclusivamente in ambito domestico e amatoriale, mentre nel
campo professionale è indispensabile avvalersi di \emph{linee bilanciate}.

\begin{figure*}[h]
  \centering
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[height=4cm]{images/unbaline2.png}
    \caption[]{Linea sbilanciata}% \\ Eq: $1(x)$}
    \label{sig:ubal2}
  \end{subfigure}%
  ~
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
    \includegraphics[height=4cm]{images/baline.png}
    \caption[]{Linea bilanciata}% \\ Eq: $0.75(x)+0.25(x\cos\theta)$}
    \label{sig:bal}
  \end{subfigure}
  \caption[]{La linea sbilanciata ha un solo conduttore, lo schermo agisce da
  negativo di ritorno e neutro. Nella linea bilanciata i due conduttori sono
  sincroni e opposti in fase, simmetrici, lo schermo è neutro.}
  \label{sig:unbal1}
\end{figure*}

La linea bilanciata è costituita dallo schermo indipendente dai due poli di
segnale, il quale viene così trasportato su due conduttori che viaggiano interni
alla schermatura. I due conduttori avranno in ogni istante valori di tensione
uguali e polarità opposta rispetto al potenziale zero dello schermo, e così,
convenzionalmente, la linea bilanciata è detta anche \emph{simmetrica}.

Per essere pienamente efficace, la linea bilanciata ha bisogno di essere
interfacciata correttamente ai due estremi. Ciò avviene secondo due tipologie:
la prima prevede la presenza di un \emph{trasformatore} in grado di accettare un
segnale bilanciato e di fornirne uno sbilanciato per le successive elaborazioni.
Il secondo tipo di interfacciamento è elettronico, e si avvale di un circuito
denominato \emph{amplificatore differenziale}, il quale preamplifica i due
segnali a fasi opposte e fornisce in uscita un segnale unico sbilanciato.
Quando nelle specifiche di un circuito compare il termine \emph{transformerless}
s’intende appunto che il dispositivo si avvale di una circuitazione elettronica.

\begin{figure*}[h]
  \centering
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
      \input{tikz/transformator}
    \caption[]{Trasformatore}% \\ Eq: $1(x)$}
    \label{sig:transf}
  \end{subfigure}%
  ~
  \begin{subfigure}[t]{0.48\textwidth}
    \centering
      \input{tikz/opamp}
      \caption[]{Amplificatore differenziale}
    \label{sig:ampdif}
  \end{subfigure}
  \caption[]{Due sistemi, trasformatore analogico ed amplificatore differenziale
  elettronico.}
  \label{sig:unbal2}
\end{figure*}

Il principio per il quale la linea bilanciata offre migliori prestazioni
rispetto alla sbilanciata è dovuto al diverso comportamento riguardo alle
interferenze esterne. La linea bilanciata può avvalersi del fenomeno della
\emph{reiezione del modo comune} (\emph{CMR, Common Mode Rejection}). Infatti,
un disturbo in radiofrequenza che andasse ad influenzare un segnale su linea
bilanciata, arriverebbe in fase all’ingresso dell’amplificatore differenziale
(o del trasformatore d’ingresso) su entrambi i poli (i quali portano il segnale
audio in opposizione di fase), venendo quindi annullato dalla caratteristica di
preamplifcazione del differenziale.

Un’altra importante prerogativa della linea bilanciata consiste nella possibilità
di alimentare i microfoni a condensatore con la tecnica del \emph{phantom powering},
inviando cioè al microfono una tensione in continua (generalmente $+48V$, sebbene
esistano microfoni in grado di accettare anche tensioni inferiori) su entrambi i
poli.

\subsection{Modello CMR}

Per comprendere a pieno l'idea alla base della cancellazione del rumore esterno
infiltrato all'interno di una connessione bilanciata ci inoltriamo in una semplice
implementazione matematica del \emph{CMR} nel linguaggio \faust\footnote{
Sal sito ufficiale \url{https://faust.grame.fr}: \itshape{Faust (Functional Audio
Stream) is a functional programming language for sound synthesis and audio
processing with a strong focus on the design of synthesizers, musical instruments,
audio effects, etc. created at the GRAME-CNCM Research Department. Faust targets
high-performance signal processing applications and audio plug-ins for a variety
of platforms and standards.

The core component of Faust is its compiler. It allows to "translate" any Faust
digital signal processing (DSP) specification to a wide range of non-domain
specific languages such as C++, C, LLVM bit code, WebAssembly, Rust, etc. In
this regard, Faust can be seen as an alternative to C++ but is much simpler and
intuitive to learn.

Thanks to a wrapping system called "architectures," codes generated by Faust can
be easily compiled into a wide variety of objects ranging from audio plug-ins to
standalone applications or smartphone and web apps, etc.}

Per l'utilizzo semplificato del linguaggio \faust~si può accedere a:
\url{http://www.faustide.grame.fr}}. Il primo passo necessario è generare un
segnale bipolare che rappresenti la sorgente sbilanciata al momento della
generazione elettrica. 

\lstinputlisting[]{faust/100/0101-CMR.dsp}


Il codice \faust~sopra espresso consiste in quattro righe di codice
\begin{compactitem}
  \item[1] linea di accesso alla libreria \emph{standard} di \faust~contenente 
  centinaia, o forse migliaia, di funzioni già scritte e accessibili nel codice
  sorgente;
  \item[2] linea di commento, il compilatore di \faust~salterà tutte le righe di
  commento che sono a sola informazione del programmatore o lettore del codice;
  \item[3] funzione \emph{uSig}, il nome della funzione è completamente arbitrario,
  avremmo potuto nominarla \emph{ciccio} senza produrre alcun effetto diverso
  in termini di risultato computazionale, fatta eccezione di un lieve sorriso.
  Ciò che è importante della funzione \emph{uSig} è situato a destra dell'uguale:
  stiamo chiedendo a \faust~ di generare una \emph{sinusoide} a $1000Hz$;
  \item[4] la riga \emph{process} nel linguaggio \faust~equivale al ciclo operativo.
  Solo le funzioni invocate nella riga \emph{process} diventano \emph{attive}.
  Queste possono essere descritte nella stessa pagina di codice in qualsiasi
  punto, anche dopo la riga \emph{process}, o prese da librerie esterne, come
  nel nostro caso per la funzione \emph{osc}, presa dalla libreria \emph{os.},
  ovvero degli \emph{oscillatori}.
\end{compactitem}

Il secondo passaggio consiste nel simulare l'uscita bilanciata del dispositivo 
sintetizzato, ovvero la generazione di un segnale simultaneo, simmetrico, a fase
invertita nei confronti della sorgente sbilanciata. 

\lstinputlisting[]{faust/100/0102-CMR.dsp}

A questo punto possiamo osservare i due segnali in uscita dal programma,
perfettamente simmetrici.

La simulazione del campo rumoroso esterno consiste in una somma di un generatore
di rumore con entrambi i segnali della linea bilanciata. È importante notare che
mentre i due segnali sono diametralmente opposti in fase, il segnale esterno
giunge con la stessa fase in somma su entrambi, modulandone l'oscillazione in
ampiezza. 

\lstinputlisting[]{faust/100/0103-CMR.dsp}

A completare l'intera simulazione manca solo l'operatore di reiezione della
banda comune che avviene \emph{semplicemente} capovolgendo la fase del segnale 
negativo (quello che viene generato in opposizione di fase al bilanciamento) che
a questo punto si trova in fase con il segnale positivo. Di conseguenza il
capovolgimento di fase andrà a ribaltare la fase del segnale rumoroso modulante
che nella somma finale sarà in opposizione di fase con se stesso, annullandosi.

\lstinputlisting[]{faust/100/0104-CMR.dsp}
