%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
%!TEX root = ../metp.tex

\section{1968. Steve REICH: \emph{Pendulum Music}.}

\begin{figure}[bh!]
	\centering
	\includegraphics[width=0.99\columnwidth]{images/pendulum-music-score.pdf}
	\caption[]{Pendulum Music. Copyright di UNIVERSAL EDITION, 1980.}
	\label{score:pendulum-music}
\end{figure}

Steve Reich compose \emph{Pendulum Music} \cite{rspm68} nel 1968. Il brano prevede
alcuni microfoni sospesi su altrettanti altoparlanti sdraiati sul pavimento,
rivolti verso l'alto. I microoni sono liberi di oscillare attraverso lo stesso
cavo di segnale che li alimenta. Ogni microfono è messo in cortocircuito con il
diffusore sottostante in modo da produrre un suono di \emph{feedback}.
Gli interpreti azionano l’oscillazione dei microfoni, il fenomeno del
\emph{feedback} avviene ad intermittenza, secondo le ritmiche dettate dal moto
oscillatorio di ogni microfono, solo quando questo si avvicina molto alla membrana
del diffusore. Il moto oscillatorio produce velocità di avvicinamento ed
allontanamento dei microfoni dal diffusore che si traduconno in piccoli e rapidi
glissando. Le pulsazioni regolari descrivono archi sempre meno ampi fino agli
ultimi movimenti attorno all’equilibrio dei microfoni che fermi sui diffusori
producono \emph{feedback} lenti, stazionari ma continuamente cangianti
influenzandosi l’un l’altro.

\begin{quote}
	If it’s done right, it’s kind of funny\footnote{
		\url{http://www.furious.com/perfect/ohm/reich.html}}.
\end{quote}

La costruzione di una pratica interpretativa di un brano elettroacustico 
essenziale, non semplice, come \emph{Pendulum Music}, essenziale negli strumenti
utilizzati, senza banalità, deve passare per una documentazione che ne renda
sostenibile l'idea, prima ancora che l'attuazione tecnologica giusta. Tuttavia le
istruzioni applicative della partitura \cite{rspm68}, seppur chiare, non
istruiscono completamente della molteplicità di risultati ottenibili una volta
azionato il processo. Ed è emblematico, proprio perchè il brano è un numero uno,
in letteratura, sotto diversi aspetti, che le parole dell'autore possano suggerire
scelte interpretative diverse in funzione delle parole con cui lo si descrive. 

\begin{quote}
	In many ways you could describe Pendulum Music as audible sculpture. The
	objects involved are the swinging microphones and the loudspeakers. I always
	set them up quite clearly as sculpture. It was important that the speakers
	be laid flat on the floor, which is obviously not usual in concerts. You
	could also call Pendulum Music a kind of performance art. It can be
	performed best on small, inexpensive loudspeakers where you get a series of
	‘bird calls’ and I much prefer that to hi-fi shriek. Pendulum Music should
	be short and amusing\footnote{Note del compositore, su disco \emph{Wergo
	WER66302}, \emph{Ensemble Avantgarde}. \url{https://www.boosey.com/cr/music/Steve-Reich-Pendulum-Music/102357}}.
\end{quote}

Una scultura, azionata da un gesto non casuale, un processo che richiede una
oculata messa in scena, per essere avviato senza ulteriori interventi nella
durata.

Visto così il brano possiede una enorme carica speculatica e pedagogica, perché
induce alla musica fisica, allo studio del processo da progettare e indirizzare
senza la possibilità di intervenire in corsa. Induce a questioni formali. Induce
soprattutto ad un ragionamento consapevole sulla messa in scena. 

\begin{quote}
	In the summer of 1968, I began thinking about what I had done musically,
	primarily about the phase pieces. I began to see them as processes as
	opposed to compositions. I saw that my methods did not involve moving from
	one note to the next, in terms of each note in the piece representing the
	composer’s taste working itself out bit by bit. My music was more of an
	impersonal process. John Cage discovered that he could take his intentions
	out of a piece of music and open up a field for many interesting things to
	happen, and in that sense I agree with him. But where he was willing to keep
	his musical sensibility out of his own music, I was not. What I wanted to do
	was to come up with a piece of music that I loved intensely, that was
	completely personal, exactly what I wanted in every detail, but that was
	arrived at by impersonal means. I com- pose the material, decide the process
	it’s going to be run through—but once these initial choices have been made,
	it runs by itself. \cite{reich:wom,reich:af}
\end{quote}

Nonostante nel 1971 Michael Nyman \cite{nyman:cw} sentenzi che il brano è l'unico 
del repertorio di Reich che può essere suonato da chiunque, anche meno “dedito e
disciplinato”, segno forse di una sua non completa comprensione della portata
musicale del processo, oggi si può ritenere che l'attuazione del processo possa
essere completa solo attraverso quelle sensibilità di relazioni tra \emph{scelta}
e \emph{ascolto} che si generano nel feedback dell'essere musicista. Il
divertimento di cui parla l'autore è realmente possibile, efficace in termini di
esperienza musicale, solo se la messa in scena è curata, i feedback differenziati
tra i vari diffusori, le pressioni acustiche in relazione con l'ambiente
circostante\ldots la fragilità della lettura di Nyman sta probabilmente nell'aver
riferito il processo alla propria visione di musica fatta di altezze e ritmi

\begin{quote}
	But in other ways Pendulum Music is uncharacteristic, since it makes as much
	a theatrical as a musical impression, and contains strong random elements of
	pitch and rhythm.
\end{quote}

Lo studio dei fenomeni naturali, la comprensione e l'articolazione di questi in
musica (fin dallo studio della corda tra i pitagorici) può scendere a diversi
livelli di profondità, mai legati al singolo fenomeno e più spesso blindati da
visioni ristrette che finiscono per restringere il campo visivo. 

La coscienza scientifica, artistica, non risiede solo nella teoria o nell'opera
specifica, ma la sua validità si misura nell'estensione temporale del pensiero e
nella crescita di questo oltre le iniziali comprensioni. 

\begin{quote}
	\textbf{MN} You’ve often referred to the lead pieces of Richard Serra.

	\textbf{SR} I would say that the relationship between Serra and me is lodged
	in Pendulum Music. I gave him the original score of Pendulum Music as a gift;
	in exchange, he gave me a piece called Candle Rack, which is simply a piece
	of wood with 10 holes drilled in it that holds candles and sits on the floor.
	Actually, my ensemble rarely performs Pendulum Music any more.

	\textbf{MN} Some of my students did it at Nottingham last year, unprompted
	by me.
	
	\textbf{SR} Really, well, it’s very easy to do, one can say that for it. I
	prefer the low-fi version. You can do it beautifully on small, inexpensive
	loudspeakers because then you get a sort of series of birdcalls and I much
	prefer that to a hi-fi shriek.

	\textbf{MN} Pendulum Music is the only piece of yours that one can talk of
	in terms of a natural process, because the other pieces, as you’ve admitted,
	all have some degree of personal intervention on your part.

	\textbf{SR} And not only that, they’re musical in the sense that Pendulum
	Music is strictly physical. A pendulum is not a musician. So of all my pieces,
	that was the most impersonal, and was the most emblematic and the most
	didactic in terms of the process idea, and also most sculptural. In many
	ways, you could describe Pendulum Music as audible sculpture, with the objects
	being the swinging microphones and the loudspeakers. I always set them up
	quite clearly as sculpture. It was very important that the speakers be laid
	flat on the floor, which is obviously not usual in concerts.
	
	\textbf{MN} So if someone composed it now, it would be called performance art.
	
	\textbf{SR} Exactly – but I’m more interested in music\footnote{
	\emph{Steve Reich: Interview} (Studio International, 192,
	November/December 1976, pp. 300–07)} \cite{nyman:cw}
\end{quote}

Così osserviamo che lo stesso Steve Reich, poco dopo la composizione del
\emph{pendulum}, non è perfettamente contemporaneo alla portata della sua opera
musicale, o forse, semplicemente, il dialogo con Nyman richiedeva un profilo
più basso.

La grande potenza della musica si esprime così, generando attitudini speculative
che possono non essere presenti nemmeno nella mente di chi architetta l'opera. Il
tempo, le sensibilità, la cultura intesa come ampliamento della comprensione del
presente attraverso uno sguardo lungo la storia, possono essere elementi sostanziali 
per affrontare l'interpretazione di un brano con attitudini che si rinnovano
sostenendo l'idea oltre la sua istantanea gittata.

In questa visione, educare musicisti a \emph{suonare} \emph{Pendulum Music} è un
atto sociale necessario, anche solo per difendere la distinzione tra interprete, 
esecutore e performer. Differenze che nell'immediato presente della discussione
tra Nyman e Reich avevano altri contorni e pesi sociali. 